import crypto from "crypto";
import mongoose from "mongoose";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please add a name"],
  },
  email: {
    type: String,
    required: [true, "Please add an email"],
    unique: true,
    Match: /^\S+@\S+\.\S+$/,
  },
  DOB: {
    type: Date,
    required: [false],
  },
  avatar: {
    type: Buffer,
    contentType: String,
    required: [false],
  },
});

export default mongoose.model("User", UserSchema);
