// Interface for response with middleware injected results
interface AdvancedResponse extends Response {
  advancedResults: string;
}
