import { NextFunction } from "express";

const asyncHandler = (fn: any) => (
  req: any,
  res: any,
  next: any
): Promise<any> => Promise.resolve(fn(req, res, next)).catch(next);

export default asyncHandler;
