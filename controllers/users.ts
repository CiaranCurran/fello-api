import asyncHandler from "../middleware/async";
import User from "../models/User";

// @desc    Create user
// @route   Post /api/v1/users
// @access  Private
export const createUser = asyncHandler(
  async (req: any, res: any, next: any) => {
    const user = await User.create(req.body);
    res.status(201).json({ success: true, data: user });
  }
);

export const getUsers = asyncHandler(async (req: any, res: any, next: any) => {
  console.log("finding users");
  res.status(200).json(res.advancedResults);
});

export const getUserById = asyncHandler(
  async (req: any, res: any, next: any) => {
    res
      .status(200)
      .json({ success: true, data: User.find({ _id: req.params.id }) });
  }
);
