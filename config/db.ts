import mongoose from "mongoose";
const colors = require("colors");

const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer;

const connectToMongo = async (uri: any) => {
  return mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });
};

const connectDB = async (): Promise<void> => {
  console.log("Connecting to database...".grey);
  if (process.env.NODE_ENV === "test") {
    // Create mock database in memory for test environment
    const mongod = new MongoMemoryServer();
    // Get mock database uri
    const uri = await mongod.getUri();
    // Connect to mock database
    const conn = await connectToMongo(uri);

    console.log(`Mock MongoDB Connected: ${conn.connection.host}`.cyan.bold);
  } else {
    // Connect to live database
    const conn = await connectToMongo(process.env.MONGO_URI);

    console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.bold);
  }
};

const closeDB = async (): Promise<void> => {
  await mongoose.disconnect();
  console.log("MongoDB Disconnected".gray.bold);
};

export { connectDB, closeDB };
