import assert from "assert";
import chai from "chai";
import request from "supertest";

// Set node environment to test
process.env.NODE_ENV = "test";
const { API_URL, API_VERSION } = process.env;

const app = require("../../server");
const { connectDB, closeDB } = require("../../config/db");

// Chai test style
const should = chai.should;

describe("GET Bootcamps", function () {
  // Connect to mock db before running tests
  before(async function () {
    await connectDB();
  });

  // Close db connection after tests have finished
  after(async function () {
    await closeDB();
  });

  it("/bootcamps should be 200", async function () {
    const res = await request(app).get(`/${API_URL}/${API_VERSION}/user`);
    assert.strictEqual(res.status, 200);
  });
});
