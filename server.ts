import colors from "colors";
import express from "express";
import dotenv from "dotenv";
import rateLimit from "express-rate-limit";
import morgan from "morgan";
import errorHandler from "./middleware/error";
import { connectDB } from "./config/db";
import { Server } from "socket.io";
import { createServer } from "http";
import formidable from "express-formidable";

// Route files
import users from "./routes/users";

// Load env vars
dotenv.config({ path: "./config/config.env" });

// Connect to database
connectDB();

// Create app instance
const app = express();

// Add body parser
app.use(express.json());
app.use(express.urlencoded());

// Dev logging middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Rate limiting
app.use(
  rateLimit({
    windowMs: 10 * 60 * 1000, // 10 mins
    max: 100,
  })
);

// Mount routers
app.use("/api/v1/users", users);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const httpServer = createServer(app);

const io = new Server(httpServer);

io.on("connection", (socket) => {
  console.log("a user connected");
  socket.on("message", (msg) => {
    console.log(
      `Message sent to chatID: ${msg.chatID}, containing the message: ${msg.message}, sent by user: ${msg.sender}`
    );
  });
});
// App.listen(5000);
httpServer.listen(5000);

// Handle unhandled promise rejections
process.on("unhandledRejection", (err: Error, promise) => {
  console.log(`Error: ${err.message}`.red);
  // Close server & exit process
  // Server.close(() => process.exit(1))
});
