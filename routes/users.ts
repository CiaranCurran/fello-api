import express from "express";
import { createUser, getUsers, getUserById } from "../controllers/users";
import User from "../models/User";
import advancedResults from "../middleware/advancedResults";

const router = express.Router();

router.route("/").post(createUser).get(advancedResults(User), getUsers);
router.route("/:id").get(getUserById);

export default router;
